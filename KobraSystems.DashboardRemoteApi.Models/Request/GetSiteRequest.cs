﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KobraSystems.DashboardRemoteApi.Models.Request
{
    public class GetSiteRequest
    {
        public string IisSiteName { get; set; }
    }
}
