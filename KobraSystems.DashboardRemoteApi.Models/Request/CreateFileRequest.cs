﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KobraSystems.DashboardRemoteApi.Models.Request
{
    public class CreateFileRequest
    {
        [Required]
        public string Path { get; set; }
        [MinLength(1)]
        public byte[] Contents { get; set; }
    }
}
