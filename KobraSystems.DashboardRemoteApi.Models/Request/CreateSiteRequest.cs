﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KobraSystems.DashboardRemoteApi.Models.Request
{
    public class CreateSiteRequest
    {
        [Required]
        public string SiteName { get; set; }
        [Required]
        public string BindingProtocol { get; set; }
        [Required]
        public string PhysicalPath { get; set; }
        [Required]
        public string ApplicationPool { get; set; }
        [Required]
        public string LogDirectory { get; set; }
        [MinLength(1)]
        public string[] Bindings { get; set; }
    }
}
