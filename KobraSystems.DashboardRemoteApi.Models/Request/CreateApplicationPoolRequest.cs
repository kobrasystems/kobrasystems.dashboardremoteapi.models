﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;

namespace KobraSystems.DashboardRemoteApi.Models.Request
{
    public class CreateApplicationPoolRequest
    {
        [Required]
        public string ApplicationPoolName { get; set; }
        public ProcessModelIdentityType ProcessModelIdentityType { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public ManagedPipelineMode ManagedPipelineMode { get; set; }
        public bool Enable32BitAppOnWin64 { get; set; }

        [Required]
        public string ManagedRuntimeVersion { get; set; }
    }
}
