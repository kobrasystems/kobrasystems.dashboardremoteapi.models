﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;
using System.Security.AccessControl;

namespace KobraSystems.DashboardRemoteApi.Models.Request
{
    public class CreateHandlerMappingRequest
    {
        [Required]
        public string IisSiteName { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Path { get; set; }
        [Required]
        public string Verb { get; set; }

        public string Type { get; set; }

        public string Modules { get; set; }
        public string ScriptProcessor { get; set; }
        public ResourceType ResourceType { get; set; }
        public RequireAccess RequireAccess { get; set; }

        public bool AllowPathInfo { get; set; }
        public string Precondition { get; set; }
        public long ResponseBufferLimit { get; set; }
        public int Index { get; set; }
    }

    public enum RequireAccess
    {
        None = 0,
        Read = 1,
        Write = 2,
        Script = 3,
        Execute = 4
    }

    public enum ResourceType
    {
        File = 0,
        Either = 1,
        Directory = 2,
        Unspecified = 3
    }
}
