﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace KobraSystems.DashboardRemoteApi.Models.Response
{
    public class RemoteApiResponse<T>
    {
        public T Body { get; set; }
        public IEnumerable<string> Warnings { get; set; } = Array.Empty<string>();
        public IEnumerable<string> Errors { get; set; } = Array.Empty<string>();


        public static RemoteApiResponse<T> Success()
        {
            return new RemoteApiResponse<T>
            {
                Body = default,
                Errors = Array.Empty<string>(),
                Warnings = Array.Empty<string>()
            };
        }

        public static RemoteApiResponse<T> Success(T body)
        {
            return new RemoteApiResponse<T>
            {
                Body = body,
                Warnings = Array.Empty<string>(),
                Errors = Array.Empty<string>()
            };
        }

        public static RemoteApiResponse<T> Error(params string[] errors)
        {
            return new RemoteApiResponse<T>
            {
                Body = default,
                Errors = errors,
                Warnings = Array.Empty<string>()
            };
        }

        public static RemoteApiResponse<T> SuccessWithWarnings(T body, params string[] warnings)
        {
            return new RemoteApiResponse<T>
            {
                Body = body,
                Errors = Array.Empty<string>(),
                Warnings = warnings
            };
        }

        public static RemoteApiResponse<T> SuccessWithWarnings(params string[] warnings)
        {
            return new RemoteApiResponse<T>
            {
                Body = default,
                Errors = Array.Empty<string>(),
                Warnings = warnings
            };
        }
    }


}
