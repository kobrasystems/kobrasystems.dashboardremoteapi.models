﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Web.Administration;

namespace KobraSystems.DashboardRemoteApi.Models.Response
{
    public class GetSiteResponse
    {
        public long Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public IEnumerable<string> Bindings { get; set; } = new string[0];
        public bool ServerAutoStart { get; set; }
        public ObjectState State { get; set; } = ObjectState.Unknown;
    }
}
